package com.example.elspoc;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.SSLContext;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.message.BasicHeader;
import org.apache.http.ssl.SSLContexts;
import org.elasticsearch.client.NodeSelector;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;

@Slf4j
public class ElsPocApplication {

  static final String HOST = "localhost";
  static final int PORT = 9200;
  static final String USER_NAME = "*"; // change it
  static final String PASSWORD = "*"; // change it

  @SneakyThrows
  public static void main(String[] args) {

    final String body = getAccountsBulkDataString();
    final RestClient restClient = configureRestClient();

    final Request writeRequest = new Request("POST", "/account/_bulk?refresh=wait_for");
//    final Request writeRequest = new Request("POST", "/account/_bulk");
    writeRequest.setJsonEntity(body);

    final long start = System.currentTimeMillis();
    final Response writeResponse = restClient.performRequest(writeRequest);
    final long end = System.currentTimeMillis();

    final Request readRequest = new Request("GET", "/account/_search");
    readRequest.setJsonEntity("{\n"
        + "\"query\":{  \n"
        + "      \"query_string\":{  \n"
        + "         \"fields\":[  \n"
        + "            \"firstname\"\n"
        + "         ],\n"
        + "         \"query\":\"*ten*\"\n"
        + "      }\n"
        + "   }\n"
        + "  \n"
        + "}");
    final Response readResponse = restClient.performRequest(readRequest);

    final Request deleteRequest = new Request("DELETE", "/account");
    final Response deleteResponse = restClient.performRequest(deleteRequest);

    log.debug(">>>>>>> Write response. Time taken {} ms. Response: {}", end - start, writeResponse);
    log.debug("<<<<<<< Read response: {}",
        new String(readResponse.getEntity().getContent().readAllBytes()));
    log.debug("<<<<<<<>>>>>>>> Cleaned up. Response {}", deleteResponse);
    restClient.close();
  }

  private static String getAccountsBulkDataString() throws IOException, URISyntaxException {
    return Files.readString(
        Path.of((ElsPocApplication.class.getClassLoader().getResource("accounts.txt").toURI())));
  }

  private static RestClient configureRestClient()
      throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {
    final CredentialsProvider credentialsProvider =
        new BasicCredentialsProvider();
    credentialsProvider.setCredentials(AuthScope.ANY,
        new UsernamePasswordCredentials(USER_NAME, PASSWORD));

    final RestClientBuilder restClientBuilder = RestClient.builder(
        new HttpHost(HOST, PORT, "https"));

    final SSLContext sslContext = SSLContexts.custom()
        .loadTrustMaterial(null, TrustAllStrategy.INSTANCE)
        .build();

    restClientBuilder.setDefaultHeaders(
        new Header[]{new BasicHeader(HttpHeaders.CONNECTION, "keep-alive")});
    restClientBuilder.setNodeSelector(NodeSelector.SKIP_DEDICATED_MASTERS);
    restClientBuilder.setHttpClientConfigCallback(httpAsyncClientBuilder ->
        httpAsyncClientBuilder
            .setDefaultCredentialsProvider(credentialsProvider)
            .setSSLHostnameVerifier((hostname, session) -> true)
            .setSSLContext(sslContext)
    );
    return restClientBuilder.build();
  }

}
